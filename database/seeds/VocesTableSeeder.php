<?php

use App\Voce;
use Illuminate\Database\Seeder;

class VocesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Voce::class, 10)->create();
    }
}
